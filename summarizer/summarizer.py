import bs4 as bs  
import urllib.request  
import re
import nltk
import heapq
import os
from nltk.tokenize.punkt import PunktSentenceTokenizer, PunktParameters
from math import log2

punkt_param = PunktParameters()
punkt_param.abbrev_types = set(['purn', 'dr', 'h', 's.h', 'pt', 'bsee', 'drs', 'hj', 'ir', 'ald'])
sentence_splitter = PunktSentenceTokenizer(punkt_param)
url_sumber = "https://id.wikipedia.org/wiki/"
stopwords_file_path = os.path.join("summarizer", "stopwords")
stopwords_file_path = os.path.join(stopwords_file_path, "stopword_list_tala.txt")
list_of_stopwords = [line.rstrip() for line in open(stopwords_file_path, "r")]
nama=[]
menteri=["menteri", "mentri", "kementerian", "kementrian"]

def setNama(nama_menteri):
    global nama
    nama = [nama.lower() for nama in nama_menteri.split("_")]



def getArticleText(nama_menteri):
    url_menteri = url_sumber + nama_menteri
    setNama(nama_menteri)
    scraped_data = urllib.request.urlopen(url_menteri)
    article = scraped_data.read()

    parsed_article = bs.BeautifulSoup(article,'lxml')

    paragraphs = parsed_article.find_all('p')

    article_text = {}
    # article_text = """
    # Sri Mulyani Indrawati, S.E., M.Sc., Ph.D (lahir di Bandar Lampung, Lampung, 26 Agustus 1962; umur 56 tahun) adalah wanita sekaligus orang Indonesia pertama yang menjabat sebagai Direktur Pelaksana Bank Dunia. Jabatan ini diembannya mulai 1 Juni 2010 hingga dia dipanggil kembali oleh Presiden Joko Widodo untuk menjabat sebagai Menteri Keuangan menggantikan Bambang Brodjonegoro, dia mulai menjabat lagi sejak 27 Juli 2016. Sebelumnya, dia menjabat Menteri Keuangan Kabinet Indonesia Bersatu. Ketika ia menjadi Direktur Pelaksana Bank Dunia maka ia pun meninggalkan jabatannya sebagai menteri keuangan saat itu. Sebelum menjadi menteri keuangan, dia menjabat sebagai Menteri Negara Perencanaan Pembangunan Nasional/Kepala Bappenas dari Kabinet Indonesia Bersatu. Sri Mulyani sebelumnya dikenal sebagai seorang pengamat ekonomi di Indonesia. Ia menjabat sebagai Kepala Lembaga Penyelidikan Ekonomi dan Masyarakat Fakultas Ekonomi Universitas Indonesia (LPEM FEUI) sejak Juni 1998. Pada 5 Desember 2005, ketika Presiden Susilo Bambang Yudhoyono mengumumkan perombakan kabinet, Sri Mulyani dipindahkan menjadi Menteri Keuangan menggantikan Jusuf Anwar. Sejak tahun 2008, ia menjabat Pelaksana Tugas Menteri Koordinator Bidang Perekonomian, setelah Menko Perekonomian Dr. Boediono dilantik sebagai Gubernur Bank Indonesia.

    # Ia dinobatkan sebagai Menteri Keuangan terbaik Asia untuk tahun 2006 oleh Emerging Markets pada 18 September 2006 di sela Sidang Tahunan Bank Dunia dan IMF di Singapura.[2] Ia juga terpilih sebagai wanita paling berpengaruh ke-23 di dunia versi majalah Forbes tahun 2008[3] dan wanita paling berpengaruh ke-2 di Indonesia versi majalah Globe Asia bulan Oktober 2007."""
    for i in range(len(paragraphs)):
        article_text[i] = paragraphs[i].text
    return article_text

def cleanArticleText(article_text):
    formatted_article_text={}
    for i in article_text.keys():
        article_text[i] = re.sub(r'\[[0-9]*\]', ' ', article_text[i])  
        article_text[i] = re.sub(r'\s+', ' ', article_text[i])

        formatted_article_text[i] = re.sub('[^a-zA-Z]', ' ', article_text[i])  
        formatted_article_text[i] = re.sub(r'\s+', ' ', formatted_article_text[i])
    return article_text, formatted_article_text


def getSentenceList(article_text):
    sentence_list={} 
    for i in article_text.keys():
        sentence_list[i] = sentence_splitter.tokenize(article_text[i])
    return sentence_list

# non_sentence = ["Dr.", "dr.", "S.", "H.", "A.", "B.", "C.", ]
# # for(i in sentence_lists):
# #     if
# print(sentence_list)
# print("\n")
# print(sentence_lists)
# word_tokens = formatted_article_text.split(" ")
# word_tokenss = nltk.word_tokenize(formatted_article_text)

# print(word_tokens)
# print("\n")
# print(word_tokenss)

def getWordFrequencies(formatted_article_text):
    word_frequencies = {}
    for paraindex in formatted_article_text.keys():
        for word in nltk.word_tokenize(formatted_article_text[paraindex]):
            if word.lower() not in list_of_stopwords:
                if word.lower() not in word_frequencies.keys():
                    word_frequencies[word.lower()] = 1
                else:
                    word_frequencies[word.lower()] += 1
    return word_frequencies

def getMaxSentenceLength(sentence_list):
    max_sentence_length = 0
    for paraindex in sentence_list.keys():
        for sentence in sentence_list[paraindex]:
            sentence_length = len(nltk.word_tokenize(sentence))
            if max_sentence_length < sentence_length:
                max_sentence_length = sentence_length
    return max_sentence_length


def getWordScore(word_frequencies, maximum_frequency):
    word_score={}
    for word in word_frequencies.keys():
        word_score[word] = (word_frequencies[word]/maximum_frequency)
        if(word in nama):
            word_score[word] += 2
        if(word.lower() in menteri):
            word_score[word] += 2
    return word_score


def getSentenceScore(sentence_list, word_score, max_sentence_length):
    sentence_scores = {} 
    para_count = 0
    for para in sentence_list.keys():
        sentence_position = 0
        for sent in sentence_list[para]:
            sent = sent.replace(":", "")
            count = 0
            sent_length = len(nltk.word_tokenize(sent))
            # if(sent_length < 60):
            for word in nltk.word_tokenize(sent.lower()):
                # if (count <= len(kodok)):
                #     if(word in kodok):
                #         if sent not in sentence_scores.keys():
                #             sentence_scores[sent] = 2
                #         else:
                #             sentence_scores[sent] += 2
                #     # elif(word in pronoun):
                #     #     if sent not in sentence_scores.keys():
                #     #         sentence_scores[sent] = 1
                if word in word_score.keys():
                    if sent not in sentence_scores.keys():
                        sentence_scores[sent] = word_score[word] * log2((sent_length+1) / (count + 1)) * log2((max_sentence_length + 1)/sent_length)
                    else:
                        sentence_scores[sent] += word_score[word] * log2((sent_length+1) / (count + 1)) * log2((max_sentence_length + 1)/sent_length)
                count+=1
            if sent not in sentence_scores.keys():
                continue
            sentence_scores[sent] += 7 * (len(sentence_list[para]) + 1 - (sentence_position + 1)) 
            sentence_position += 1
            sentence_scores[sent] += 5 * (len(sentence_list.keys()) + 1 - (para))
    print(sentence_scores)
    print(heapq.nlargest(4, sentence_scores.values()))

            
    return heapq.nlargest(4, sentence_scores, key=sentence_scores.get)

def evaluateSentenceScores(nama_menteri):
    #HIgh couplingg
    article_text = getArticleText(nama_menteri)
    article_text, formatted_article_text = cleanArticleText(article_text)
    sentence_list = getSentenceList(article_text)
    word_frequencies = getWordFrequencies(formatted_article_text)
    maximum_word_frequency = max(word_frequencies.values())
    maximum_sentence_length = getMaxSentenceLength(sentence_list)
    word_score = getWordScore(word_frequencies, maximum_word_frequency)
    highest_sentence = getSentenceScore(sentence_list, word_score, maximum_sentence_length)
    summary = ' '.join(highest_sentence)
    return summary



# print(heapq.nlargest(4, sentence_scores.values()))
# summary_sentences = heapq.nlargest(4, sentence_scores, key=sentence_scores.get)

# summary = ' '.join(summary_sentences)  
# print(summary)  
