from django.shortcuts import render
from .models import Menteri
from .summarizer import evaluateSentenceScores

# Create your views here.
response = {}
def index(request):
	menteris = Menteri.objects.all()
	response["menteris"] = menteris

	html = "pilih-menteri.html"
	return render(request, html, response)

def show_summarize(request, id):
	menteri = Menteri.objects.get(id=id)

	response['menteri'] = menteri
	nama_underscore = menteri.getUnderscore()
	summary = evaluateSentenceScores(nama_underscore)
	response['summary'] = summary

	html = 'show-summarize.html'
	return render(request, html, response) 	