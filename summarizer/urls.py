from django.conf.urls import url

from .views import *

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^(?P<id>\w{0,50})/$', show_summarize, name='show_summarize'),
]