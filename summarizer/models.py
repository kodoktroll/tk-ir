from django.db import models

# Create your models here.
class Menteri(models.Model):
	nama = models.CharField(max_length=50)
	foto = models.CharField(max_length=300)

	def __str__(self):
		return self.nama
		
	def getUnderscore(self):
		name_with_underscore = self.nama.replace(" ", "_")
		return name_with_underscore